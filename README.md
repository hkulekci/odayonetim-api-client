# Odayonetim API Client

This client provide an interface to reach Odayonetim API service.

## Installation

You need to use composer for the installing

```
composer require hkulekci/odayonetim-api-client
```

## Configuration

Please check the `examples` folder for more information.

```
$configuration = \OdayonetimApiClient\ApiConfiguration::createFromArray([
    'base_uri' => 'http://127.0.0.1:8080/',
    'client_id' => 'myawesomeapp',
    'client_secret' => 'whisky'
]);
$client = new \OdayonetimApiClient\HttpClient(
    new \GuzzleHttp\Client(['timeout'  => 5.0,]),
    $configuration
);
```

The client ready without token. You need to get token.

```
$result = $client->getPasswordAccessToken('test', 'demo')->getBody();
if (!isset($result['access_token'])) {
    echo 'Please provide correct information to get token' . PHP_EOL;
}
$access_token = $result['access_token'];
$client->setToken($access_token);
```

Now you can use all the methods to get data from API.
