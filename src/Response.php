<?php
namespace OdayonetimApiClient;

use GuzzleHttp\Exception\ClientException;

class Response
{
    protected $statusCode;
    protected $body;

    public function setStatusCode(int $statusCode): Response
    {
        $this->statusCode = $statusCode;

        return $this;
    }

    public function getStatusCode(): int
    {
        return $this->statusCode;
    }

    public function setBody($body): Response
    {
        $this->body = $body;

        return $this;
    }

    public function getBody(): array
    {
        return \json_decode($this->body, true);
    }

    public static function createByGuzzleException(ClientException $e): self
    {
        $response = new self();
        $response->setStatusCode($e->getResponse()->getStatusCode());
        $response->setBody($e->getResponse()->getBody());

        return $response;

    }

    public static function createByGuzzleResponse(\GuzzleHttp\Psr7\Response $psrResponse): self
    {
        $response = new self();
        $response->setStatusCode($psrResponse->getStatusCode());
        $response->setBody($psrResponse->getBody()->getContents());

        return $response;
    }
}
