<?php
namespace OdayonetimApiClient;

use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Psr7\Request;

class HttpClient
{
    protected $client;

    protected $configuration;

    protected $token;

    public function __construct(
        ClientInterface $client,
        ApiConfiguration $configuration
    ) {
        $this->client = $client;
        $this->configuration = $configuration;
    }

    public function getConfiguration(): ApiConfiguration
    {
        return $this->configuration;
    }

    private function getToken(): ?string
    {
        return $this->token;
    }

    public function setToken($token): HttpClient
    {
        $this->token = $token;

        return $this;
    }

    public function getPasswordAccessToken(string $username, string $password):Response
    {
        $request = new Request(
            'POST',
            $this->getConfiguration()->getBaseUri() . 'access_token',
            [
                'Content-Type' => 'application/x-www-form-urlencoded',
            ],
            http_build_query(
                [
                    'grant_type'    => 'password',
                    'client_id'     => $this->getConfiguration()->getClientId(),
                    'client_secret' => $this->getConfiguration()->getClientSecret(),
                    'scope'         => 'basic',
                    'username'      => $username,
                    'password'      => $password,
                ],
                '',
                '&'
            )
        );

        return $this->executeRequest($request);
    }

    public function get($path, $headers = [], $parameters = [])
    {
        $queryParams = '';
        if ($parameters) {
            $queryParams = '?' . http_build_query($parameters);
        }

        $headers = array_merge([
            'Accept' => 'application/json',
            'Content-Type' => 'application/json',
        ], $headers);

        $request = new Request(
            'GET',
            $this->getConfiguration()->getBaseUri() . $path . $queryParams,
            $headers
        );

        return $this->executeRequest($request);
    }

    public function post($path, $headers = [], $data = [])
    {
        $headers = array_merge([
            'Accept' => 'application/json',
            'Content-Type' => 'application/json',
        ], $headers);

        $request = new Request(
            'POST',
            $this->getConfiguration()->getBaseUri() . $path,
            $headers,
            json_encode($data)
        );

        return $this->executeRequest($request);
    }

    public function delete($path, $headers = [])
    {
        $headers = array_merge([
            'Accept' => 'application/json',
            'Content-Type' => 'application/json',
        ], $headers);

        $request = new Request(
            'DELETE',
            $this->getConfiguration()->getBaseUri() . $path,
            $headers
        );

        return $this->executeRequest($request);
    }

    private function executeRequest(Request $request): Response
    {
        try {
            if ($this->getToken()) {
                $request = $request->withHeader(
                    'Authorization', 'Bearer ' . $this->getToken()
                );
            }
            $response = $this->client->send($request);
            return Response::createByGuzzleResponse($response);
        } catch (ClientException $e) {
            return Response::createByGuzzleException($e);
        } catch (GuzzleException $e) {
            throw new \RuntimeException('Please check your configuration!');
        } catch (\Exception $e) {
            $response = new Response();
            $response->setStatusCode(500);
            $response->setBody('{"message": "'.$e->getMessage().'"}');
            return $response;
        }
    }
}
