<?php
namespace OdayonetimApiClient;

class ApiConfiguration
{
    private $baseUri;
    private $clientId;
    private $clientSecret;

    private function __construct(
        string $baseUri,
        string $clientId,
        string $clientSecret
    ) {
        $this->baseUri = $baseUri;
        $this->clientId = $clientId;
        $this->clientSecret = $clientSecret;
    }

    public function getBaseUri(): string
    {
        return $this->baseUri;
    }

    public function getClientId(): string
    {
        return $this->clientId;
    }

    public function getClientSecret(): string
    {
        return $this->clientSecret;
    }

    public static function createFromArray(array $data): self
    {
        if (!isset($data['base_uri'])) {
            $data['base_uri'] = '';
        }

        if (!isset($data['client_id'])) {
            $data['client_id'] = '';
        }

        if (!isset($data['client_secret'])) {
            $data['client_secret'] = '';
        }

        return new self(
            $data['base_uri'],
            $data['client_id'],
            $data['client_secret']
        );
    }
}
