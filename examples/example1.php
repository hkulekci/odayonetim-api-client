<?php
require __DIR__.'/../vendor/autoload.php';

$configuration = \OdayonetimApiClient\ApiConfiguration::createFromArray([
    'base_uri' => 'http://127.0.0.1:8080/',
    'client_id' => 'myawesomeapp',
    'client_secret' => 'whisky'
]);
$client = new \OdayonetimApiClient\HttpClient(
    new \GuzzleHttp\Client(['timeout'  => 5.0,]),
    $configuration
);

$result = $client->getPasswordAccessToken('test', 'demo')->getBody();
if (!isset($result['access_token'])) {
    var_dump($result);
    echo 'Please provide correct information to get token' . PHP_EOL;
}
$access_token = $result['access_token'];
$client->setToken($access_token);


$me = $client->get('me')->getBody();
echo 'Welcome ' . $me['data']['person']['nameSurname'] . PHP_EOL;

$branch = $client->get('branch/1')->getBody();
echo 'Branch 1 is ' . $branch['data']['name'] . PHP_EOL;

$newBranch = $client->post('branch', [], [
    'name' => 'Hakkari Şubesi',
    'shortName' => 'Hakkari',
    'code' => 1
]);

var_dump($newBranch->getBody());
